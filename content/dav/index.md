---
title: "DAV-Korrespondent für Stochastik"
description: ""
---

{{% leftalign %}}
Ich stelle Bescheinigungen über Grundkenntnisse in Stochastik für die Zulassung zur Aktuarausbildung aus, sofern diese an der Universität Göttingen erworben wurden. In der Regel muss dazu die Bachelorvorlesung Maß- und Wahrscheinlichkeitstheorie, sowie eine Vorlesung in (mathematischer) Statistik erfolgreich besucht worden sein. Siehe Anhang B der Lernziele im Grundwissen für die genaueren Inhalte.

Bitte senden Sie mir eine Kopie Ihres Bachelor- und/oder Masterzeugnisses zur Bestätigung und geben Sie an, in welchem Semester (z.B. WiSe 2021/22) die relevanten Vorlesungen besucht wurden. Sämtliche zugesandten Dokumente werden nach Abschluss des Vorgangs umgehend gelöscht.
{{% /leftalign %}}
