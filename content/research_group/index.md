---
title: "Research Group"
description: "Master's/PhD students and Postdocs"
layout: "simple"
---


{{% leftalign %}}

## Current members

* Marianne Abemgnigni Njifon, M.Sc.  
PhD topic: Neural network models for spatial data 

* Leoni Wirth, M.Sc.  
PhD topic: Stein's method for random graph distributions 


---

## Former members

### Postdoc

* Dr. Viktor Bezborodov 


### PhD students

<ul>
<li>Dr. Raoul Müller<br>
  Barycenters and ANOVA for Point Pattern Data<br>
  joint supervision with <a href="https://www.mathematik.uni-kl.de/en/opt/people/head/schoebel/">Prof. A. Sch&ouml;bel</a>
</li>

<li>Dr. Johannes Wieditz<br>
  Characteristic and Necessary Minutiae in Fingerprints (2021)<br>
  joint supervision with <a href="https://stochastik.math.uni-goettingen.de/index.php?id=14&username=huckeman">Prof. Stephan Huckemann</a></li>

<li>Dr. Henning Höllwarth<br>
  Regularized Rao–Blackwellization – An Extension of a Classical Technique with Applications to Gibbs Point Process Statistics (2020)
</li>

<li>Dr. Jörn Schrieber<br>
  Algorithms for Optimal Transport and Wasserstein Distances (2019)<br>
  joint supervision with Prof. Anita Schöbel
</li>

<li>Dr. Garyfallos Konstantinoudis<br>
  Analysis of Clustering of Childhood Cancers (2019)<br>
  joint supervision with PD Ben Spycher, PhD, Universität Bern
</li>

<li>Dr. Fabian Kück<br>
  Convergence Rates in Dynamic Network Models (2017)<br>
</li>

<li>Dr. Kaspar Stucki<br>
  Invariance Properties and Approximation Results for Point Processes (2013)<br>
  joint supervision with Prof. Ilya Molchanov, Universität Bern
</li>
</ul>
{{% /leftalign %}}
