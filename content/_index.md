---
title: "Homepage of Dominic Schuhmacher"
description: "Main information"
date: "2025-02-20"

cascade:
  showDate: false
  showAuthor: false
  showSummary: true
  invertPagination: false
---


<div class="text-xl">
{{% markdown %}}
Theory, statistics and algorithms for spatial stochastic processes  
Computational statistics and simulation  
[Statistics in metric spaces and similarity spaces](https://www.kanjidist.org/)
{{% /markdown %}}
</div>

{{% leftalign %}}
I'm currently head of the [Institute for Mathematical Stochastics](https://www.stochastik.math.uni-goettingen.de/index.php?id=home&language=en).

I supervise Bachelor's and Master's theses in theoretical, computational and applied topics on spatial stochastic processes and statistics on metric spaces. [Please read here]({{< ref "for_students#bachelors-and-masters-theses" >}}) before you contact me.

I'm also the DAV correspondent for stochastics. [Read here]({{< ref "dav" >}}) (in German) if you have studied in Göttingen and require confirmation on basic knowledge in stochastics for starting your actuary studies.
{{% /leftalign %}}
