---
title: "Software"
description: ""
---

### `pppdist` and related functions
for working with inter-point-pattern distances as a contribution to the R package<br>
[Spatstat: Spatial Point Pattern Analysis, Model-Fitting, Simulation, Tests](https://spatstat.org/)  
 
  [![Optimal point pairing](img/ospa_sm.png)](img/ospa.svg)


---

### R package `logconcens`
Maximum Likelihood Estimation of a log-Concave Density Based on Censored Data

* [`logconcens` package on CRAN](https://cran.r-project.org/package=logconcens) 
* [Presentation at the Swiss Statistics Meeting 2011 in Fribourg](talks/fribourg11.pdf)
* [Dümbgen, Rufibach and S (2014)](https://dx.doi.org/10.1214/14-EJS930)

---

### R Package `transport`
Computation of Optimal Transport Plans and Wasserstein Distances

* [`transport` package on GitHub](https://github.com/dschuhmacher/transport) 
* [`transport` package on CRAN](https://cran.r-project.org/package=transport)

  | [![OT for Euclidean distance](img/ot64_1_sm.png)](img/ot64_1.svg) | [![OT for squared Euclidean distance](img/ot64_2_sm.png)](img/ot64_2.svg) | [![Semidiscrete OT for Euclidean distance](img/ot64_semi1_sm.png)](img/ot64_semi1.svg) |
  |:----------:|:----------:|:----------:|
  |            |            |            | 

### R Package `ttbary`
Barycenter Methods for Spatial Point Patterns 

* [`ttbary` package on CRAN](https://cran.r-project.org/package=ttbary) 
* [Müller, S and Mateu (2020)](https://doi.org/10.1007/s11222-020-09932-y)

---

### R Package `kanjistat`
A Statistical Framework for the Analysis of Japanese Kanji Characters

* [`kanjistat` website (GitHub)](https://dschuhmacher.github.io/kanjistat/)
* [`kanjistat` package on CRAN](https://cran.R-project.org/package=kanjistat)
* [S (2023)](https://doi.org/10.48550/arXiv.2304.02493)
* [Interactive visualization of kanji distances](https://www.kanjidist.org)
  
  | [![Kanji with components in different colors](img/hair_kveckanji_sm.svg)](img/hair_kveckanji.svg) | [![Dendrogram of the kanji](img/hair_kvecdend_sm.svg)](img/hair_kvecdend.svg) |
  |:----------:|:----------:|

  | [![Ink transport source and destination](img/treestrans_sm.png)](img/treestrans.svg) | [![Optimal ink transport](img/treesplan_sm.png)](img/treesplan.svg) |
  |:----------:|:----------:|


### R Package `graphmetrics`
Assignment Based Metrics for Attributed Graphs

* [`graphmetrics` package on GitHub](https://github.com/dschuhmacher/graphmetrics)
* [S and Wirth (2023)](https://doi.org/10.48550/arXiv.2308.12165)
