---
title: "Information for Students"
description: "The path to a Bachelor's or Master's thesis"
layout: "simple"
---

<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js"></script>
<script>
  MathJax = {
    tex: {
      displayMath: [['\\[', '\\]'], ['$$', '$$']], 
      inlineMath: [['\\(', '\\)'], ['$', '$']] 
    }
  };
</script>


## Lecture cycle

Every two to three years I give a three-semester lecture cycle on spatial stochastics. This cycle covers mainly the **probability theory** of random objects in space. A large part of this theory is built on point processes (random point clouds in $\mathbb{R}^d$ or a more general space). In the accompanying seminars we treat complementary topics of **stochastic simulation** and **statistics** of objects in space.

### SpatStoch I (WiSe 2023/24)

- Advanced probability theory
- Markov chains (discrete time and space), pure-jump Markov processes (continuous time, discrete space)
- Introduction to point processes in $\mathbb{R}^d$
- Introduction to random fields (random functions on $\mathbb{R}^d$)

### SpatStoch II (SoSe 2024, mostly accessible without SpatStoch I)

- All the important theory of point processes on general state spaces
- Point processes of functions and sets
- Gibbs point processes on $\mathbb{R}^d$

### SpatStoch III (planned for WiSe 2024/25, needs SpatStoch II)

- Markov chains and pure-jump Markov processes on a continuous space
- Spatial birth-and-death processes
- Random tessellations
- Random sets


## Bachelor's and Master's theses

If you feel inclined to write a thesis under my supervision, please write me an e-mail some weeks before the intended starting date specifying your previous knowledge as well as your ideas and expectations regarding your thesis project. The latter can be quite rough, e.g., whether you would rather like to work on a purely theoretic thesis topic or also include applied aspects and/or substantial programming work. It is also helpful to know what topic(s) you particularly liked in the courses and seminars you took. I will then think of a tentative topic and we can fix a meeting.

### Bachelor theses

Ideally, I know you from one of my courses or seminars. If not, please state the motivation for writing a thesis under my supervision.

### Master theses

In most cases, I strictly require that you have successfully passed SpatStoch II or have proven knowledge in spatial stochastic theory from elsewhere. It is possible to make up for this by working through the lecture notes of SpatStochII on your own, but please be aware that this may be hard and time-consuming. 

An alternative is if you already have a more or less concrete idea for a thesis project and sound general mathematical and/or statistical knowledge, but require skills in an area of my expertise (including spatial probability/statistics, stochastic simulation, computational statistics). Then I am happy to fix a meeting where you can pitch your idea to me.


## Former thesis topics

Below is the complete(?) list of theses I have supervised. The names of the students have been omitted due to European data protection laws. If you are the author of one of the theses below, I’m more than happy to add your name to your work (and possibly a link) if you give me explicit permission to do so.

### Master

<ul>
<li>Iterative Boltzmann Inversion — A spatial statistics perspective (2023)</li>
<li>Theory for Hamiltonian MCMC algorithms for Gibbs processes (2023)
</li>
<li>The Implementation of a Risk Analysis in the Reinsurance Sector using Copulas (2022);
  joint supervision with Prof. Michael Fröhlich
</li>
<li>Structural Inference for Temporal Knowledge Graphs: a Deep Learning Method and a Stochastic Theory Framework (2022)
</li>
<li>Pricing Approaches for the Insurance Division Aviation in Primary Insurance and Reinsurance (2022);
  joint supervision with Prof. Michael Fröhlich
</li>
<li>Spatial Modelling of Gaussian Markov Random Fields using INLA and SPDEs (2021)
</li>
<li>Uniqueness of Gibbs Measures: Sufficient Conditions (2021)
</li>
<li>Estimation of Photovoltaic-Generated power: Convolutional Neural Network vs Kriging (2021)
</li>
<li>Varianzanalyse in euklidischen und nichteuklidischen metrischen Räumen (2021)
</li>
<li>Wasserstein Learning for Generative Point Process Models (2020)
</li>
<li>Uniqueness of Gibbs measures via Disagreement Percolation (2020)
</li>
<li>Binned Estimation of the Pair Correlation Function and Iterative Boltzmann Inversion (2020)
</li>
<li>A Probabilistic Look at Mutual Information with Application to Point Process (2017)
</li>
<li>Selective Importance Sampling for Computing the Maximum Likelihood Estimator in Point Process Models (2017)
</li>
<li>Convergence Rates for Point Processes Thinned by Logit-Gaussian Random Fields (2016)
</li>
<li>Maximum-Likelihood-Schätzung von exponentiellen Familien von stochastischen Prozessen (2016)
</li>
<li>Maximum Likelihood Estimation for Spatial Point Processes using Monte Carlo Methods (2016)
</li>
<li>Statistical Inference of Linear Birth-And-Death Processes (2015)
</li>
<li>Konvergenzgeschwindigkeit für Markov-Chain Monte Carlo (2015)
</li>
<li>Tests auf Unabhängigkeit zwischen Punkten und Marken (2015)
</li>
<li>Thinning of Point Processes by [0,1]-Transformed Gaussian Random Fields (2014)
</li>
<li>Additivity and Ortho-Additivity in Gaussian Random Fields (2013);
  joint supervision with Prof. David Ginsbourger
</li> 
</ul>

### Bachelor

<ul>
<li>An Application of Space-Time Point Processes: The ETAS-Model for Earthquake Prediction (2025)
</li>
<li>Konvergenzraten auf Grundlage der Minorisierungsbedingung für Markovketten auf abzählbarem Zustandsraum (2024)
</li>
<li>Continuum percolation theory (2024)
</li>
<li>Predictability of PRNGs using neural networks (2024)
</li>
<li>Exploring Spatio-Temporal Kriging: Theory and Application to Nitrogen Dioxide Data in the Greater Frankfurt Area (2023)
</li>
<li><a href="https://github.com/file-acomplaint">Lennart Finke</a>, <a href="https://publications.goettingen-research-online.de/handle/2/142996">Markov Models for Spaced Repetition Learning (2023)</a>;
joint supervision with Prof. Anja Sturm
</li>
<li>Noisy Hamiltonian Monte Carlo with an application for point processes (2023)
</li>
<li>Maximum Likelihood Estimation for Hawkes Processes and Real Data Application (2022)
</li>
<li>Entrywise Relative Error Bounds for the Stationary Distribution of Perturbed Markov Chains on a Finite State Space. (2022)
</li>
<li>Second Order Moment Measures of Point Processes (2022)
</li>
<li>Obere Schranken bei der Bewertung von Stoploss-Verträgen in der Rückversicherung (2022); 
  gemeinsame Betreuung mit Prof. Michael Fröhlich
</li>
<li>Statistical Analysis of Simulation Algorithms for Finite Random Fields &mdash; with a Focus on the Swendsen-Wang Algorithm for the Ising Model (2022)
</li>
<li>Sequential Monte Carlo Methods and Their Applications in Stock Markets (2022)
</li>
<li>Comparison of Metropolis Chain and Glauber Dynamics for Proper q-Colorings on a Graph (2021)
</li>
<li>Das Tobit-Modell: Methodische Anwendungen und Vergleiche zu linearen Regressionen (2021)
</li>
<li>Markov-Ketten mit allgemeinem Zustandsraum (2021)
</li>
<li>A comparison between Metropolis–Hastings and Hamiltonian Monte Carlo (2020)
</li>
<li>Vorhersage im Besag-York-Mollié-Modell (2018)
</li>
<li>Spline-Regression (2018)
</li>
<li>Nichtparametrische Regression - Kernregression und lokale Polynome (2018)
</li>
<li>Theorie und Simulation von Gaußschen Markov-Zufallsfeldern (2018)
</li>
<li>Comparison of Logistic Regression and Maximum Pseudolikelihood for Spatial Point Processes (2017)
</li>
<li>Metropolis-Hastings Algorithms for Spatial Point Processes (2016)
</li>
<li>Valentin Hartmann, <a href="https://arxiv.org/abs/1706.07403">A Geometry-Based Approach for Solving the Transportation Problem with Euclidean Cost (2016)</a>
</li>
<li>A Comprehensive Overview of Linear Birth-and-Death Processes with an Outlook to the Non-Linear Case (2015)
</li>
<li>Limit Behaviour of Discrete Models in Financial Mathematics (2015)
</li>
<li>Gaußsche Zufallsfelder: Differenzierbarkeit von Pfaden (2015)
</li>
<li>Shuffling Measures and the Total Variation Distance to a Perfectly Randomized Deck of Cards (2015)
</li>
<li>Numerical Computation of L<sub>2</sub>-Wasserstein Distance Between Images (2014)
</li>
<li>Erwartete Treffzeiten in Markovketten und deren Anwendung auf Glücksspiele mit Sicherungsoption (2013)</li>
</ul>


