---
title: "Links"
description: "Professional and personal interests"
layout: "simple"
---

{{% leftalign %}}
<p>The following is a very subjective collection of links centered around some professional and personal interests of mine.</p>

<h4>Maths, Stats and Programming</h4>

<a href="http://www.stochastik.math.uni-goettingen.de">IMS Göttingen</a><br>
<a href="http://www.math.uni-goettingen.de">Mathematik an der Universität Göttingen</a><br>
<a href="http://www.r-project.org/">R-project</a><br>
<a href="https://posit.co/">Posit</a><br>
<a href="https://huggingface.co/">Hugging Face</a><br>
<a href="https://teuder.github.io/rcpp4everyone_en/">Rcpp for everyone</a><br>
<a href="https://mc-stan.org">Stan</a><br>
<a href="https://www.cgal.org">CGAL</a><br>
<a href="http://spatstat.github.io">Spatstat</a><br>
<a href="https://www.onlinegdb.com">OnlineGDB</a><br>
<a href="https://regexr.com">regexr</a><br>
<!--<a href="http://www.spatstat.ch">spatstat.ch</a>/<a href="http://www.spatialstatistics.ch">spatialstatistics.ch</a><br>-->

<h4>General</h4>
<a href="https://chat.openai.com">Chat GPT</a><br>
<a href="https://www.theguardian.com/international">The Guardian</a><br>
<a href="https://www.quantamagazine.org">Quanta Magazine</a><br>
<a href="http://www.heise.de/">Heise Online</a><br>
<a href="http://dict.leo.org/">LEO English &lt;--&gt; German Dictionary</a><br>
<a href="https://www.urbandictionary.com/">Urban Dictionary</a><br>
<a href="http://dict.leo.org/?lp=frde&amp;search=">LEO French &lt;--&gt; German Dictionary</a><br>
<a href="https://www.languefrancaise.net/Bob">Bob: dictionnaire d'argot</a><br>
<a href="https://www.deepl.com/translator">DeepL Translator</a><br>
<!--<a href="https://tatoeba.org/en">Tatoeba</a><br>-->
<a href="http://reiseauskunft.bahn.de">Reiseauskunft DB</a><br>

<h4>Göttingen</h4>
<a href="http://www.sub.uni-goettingen.de/">SUB Göttingen</a><br>
<a href="https://www.goevb.de">GöVB</a><br>

<h4>Switzerland</h4>
<a href="https://www.srf.ch">SRF</a><br>
<a href="https://opendata.swiss/de">opendata.swiss</a><br>
<a href="https://www.idiotikon.ch">Idiotikon</a><br>

<h4>Japan</h4>
<a href="https://jisho.org/">Jisho - Online Dictionary</a><br>
<a href="https://jotoba.de">Jotoba - Online Dictionary</a><br>
<a href="https://taximanli.github.io/kotobade-asobou/">言葉で遊ぼう - Japanese Wordle</a><br>
<a href="https://www.aozora.gr.jp">青空文庫 - Public Domain Texts</a><br>
<a href="https://www.e-stat.go.jp">e-Stat - 政府統計の総合窓口</a><br>
<a href="https://www.ninjal.ac.jp/english/">国立国語研究所 - NINJAL</a><br>
<a href="http://codh.rois.ac.jp">ROIS-DS Center for Open Data in the Humanities</a><br>
<a href="https://learnnatively.com/">Natively - graded Japanese books and more</a><br>
<a href="https://japaneselit.net/">Contemporary Japanese Literature Blog</a><br>
<a href="https://jfdb.jp/en/">Japanese Film Database</a><br>
<a href="https://www.justonecookbook.com/">Just One Cookbook</a><br>
<a href="https://www3.nhk.or.jp/nhkworld/">NHK World Japan</a><br>
<a href="https://www3.nhk.or.jp/news/easy/">NHK News Web Easy</a><br>
<a href="https://www.japantimes.co.jp">The Japan Times</a><br>
<a href="https://sumikai.com">Sumikai</a><br>
{{% /leftalign %}}
