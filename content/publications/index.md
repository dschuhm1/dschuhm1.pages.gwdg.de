---
title: "Publications"
description: "Papers and Preprints"
layout: "simple"
---

<ul>
  <li class="vspace">
    Marianne Abémgnigni Njifon, Tobias Weber, Viktor Bezborodov, Tyll Krueger and DS (2025).<br>
    <a href="https://doi.org/10.48550/arXiv.2502.05458">Block Graph Neural Networks for tumor heterogeneity prediction</a>,
    arXiv preprint.
  </li>

  <li class="vspace">
    DS and Leoni Carla Wirth (2024).
    <a href="https://doi.org/10.48550/arXiv.2411.02917">Stein's Method for Spatial Random Graphs</a>,
    arXiv preprint.
  </li>

  <li class="vspace">
Raoul Müller, DS and <a href="https://www3.uji.es/~mateu/">Jorge Mateu</a> (2024).
  <a href="https://projecteuclid.org/journals/statistical-science/volume-39/issue-2/ANOVA-for-Metric-Spaces-with-Applications-to-Spatial-Data/10.1214/23-STS898.full">ANOVA for metric spaces, with applications to spatial data</a>,<br>
<i>Statistical Science</i> <b>39</b>(2), 262-285.
  </li>

  <li class="vspace">
    Marianne Abémgnigni Njifon and DS (2024).
    <a href="https://doi.org/10.1016/j.spasta.2024.100822">Graph convolutional networks for spatial interpolation of correlated data</a>,<br>
    <i>Spatial Statistics</i> <b>60</b>, 100822.
  </li>

  <li class="vspace">
    DS and Leoni Carla Wirth (2023).
    <a href="https://doi.org/10.48550/arXiv.2308.12165">Assignment Based Metrics for Attributed Graphs</a>,
    arXiv preprint.
  </li>

  <li class="vspace">
    DS (2023).
    <a href="	https://doi.org/10.48550/arXiv.2304.02493">Distance maps between Japanese kanji characters based on hierarchical optimal transport</a>,
    arXiv preprint.
  </li>

  <li class="vspace">
    Raoul Müller, <a href="https://www.mathematik.uni-kl.de/opt/personen/leitung/schoebel">Anita Schöbel</a>, DS (2023).
    <a href="https://doi.org/10.1142/S0217595922500452">Location problems with cutoff</a>,<br>
<i>Asia-Pacific Journal of Operational Research</i> <b>40</b>(3), 2250045.
  </li>

  <li class="vspace"><a href="https://www.stochastik.math.uni-goettingen.de/index.php?id=14&username=wieditz">Johannes Wieditz</a>, <a href="https://www.ucl.ac.uk/statistics/people/yvopokern">Yvo Pokern</a>, DS and <a href="https://stochastik.math.uni-goettingen.de/index.php?id=14&username=huckeman">Stephan Huckemann</a> (2022).<br>
    <a href="https://doi.org/10.1111/rssc.12520">Characteristic and necessary minutiae in fingerprints</a>,
    <i>J R Stat Soc Series C</i> <b>71</b>, 27–50.
  </li>

<li class="vspace"><a href="https://www.imperial.ac.uk/people/g.konstantinoudis">Garyfallos Konstantinoudis</a>, DS, Roland Ammann, Tamara Diesch, Claudia Kuehni and <a href="http://www.ispm.unibe.ch/about_us/staff/spycher_ben/index_eng.html">Ben Spycher</a> (2020).<br>
  <a href="https://ij-healthgeographics.biomedcentral.com/articles/10.1186/s12942-020-00211-7">Bayesian spatial modelling of childhood cancer incidence in Switzerland using exact point data: a nationwide study during 1985–2015</a>, <i>International Journal of Health Geographics</i> <b>19</b>(15).</li>

<li class="vspace"><a href="https://www.stochastik.math.uni-goettingen.de/index.php?id=14&username=mueller647">Raoul M&uuml;ller</a>, DS and <a href="https://www3.uji.es/~mateu/">Jorge Mateu</a> (2020).
  <a href="https://doi.org/10.1007/s11222-020-09932-y">Metrics and barycenters for point pattern data</a>,<br>
<i>Statistics and Computing</i> <b>30</b>, 953-972.</li>

<li class="vspace"><a href="https://people.epfl.ch/valentin.hartmann?lang=en">Valentin Hartmann</a> and DS (2020).<br>
  <a href="http://link.springer.com/article/10.1007/s00186-020-00703-z">Semi-discrete optimal transport: a solution procedure for the unsquared Euclidean distance case</a>,<br>
  <i>Mathematical Methods of Operations Research</i> <b>92</b>, 133-163.</li>

<li class="vspace"><a href="http://www.stochastik.math.uni-goettingen.de/index.php?id=14&module=Persondetails&range_id=db66fd7fdd6564055fadd6873d86b790&username=fabian_kueck">Fabian Kück</a> and DS (2020).
  <a href="http://arxiv.org/abs/1509.04918">Convergence rates for the degree distribution in a
dynamic network model</a>, <br>
<i>Stochastic Models</i> <b>36</b>(1), 134-171.  [<a href="https://www.tandfonline.com/doi/full/10.1080/15326349.2019.1696685">Published version</a>]</li>

<li class="vspace"><a href="https://www.imperial.ac.uk/people/g.konstantinoudis">Garyfallos Konstantinoudis</a>, DS, <a href="https://www.kaust.edu.sa/en/study/faculty/haavard-rue">H&aring;vard Rue</a> and <a href="http://www.ispm.unibe.ch/about_us/staff/spycher_ben/index_eng.html">Ben Spycher</a> (2020).<br>
  <a href="https://arxiv.org/abs/1808.04765">Discrete versus continuous domain models for disease mapping</a>, <i>Spatial and Spatio-Temporal Epidemiology</i> <b>32</b>, 100319.  [<a href="https://doi.org/10.1016/j.sste.2019.100319">Published version</a>]</li>

<li class="vspace"><a href="https://www.nt.tuwien.ac.at/about-us/staff/guenther-koliander/">G&uuml;nther Koliander</a>, DS and <a href="https://www.nt.tuwien.ac.at/about-us/staff/franz-hlawatsch/">Franz Hlawatsch</a> (2018).
  <a href="https://arxiv.org/abs/1704.05758">Rate-distortion theory of finite point processes</a>,<br>
  <i>IEEE Trans. Inf. Theory</i> <b>64</b>(8), 5832-5861. [<a href="https://doi.org/10.1109/TIT.2018.2829161">Published version</a>]</li>

<li class="vspace"><a href="http://www.stochastik.math.uni-goettingen.de/index.php?id=14&module=Persondetails&range_id=db66fd7fdd6564055fadd6873d86b790&username=fabian_kueck">Fabian Kück</a> and DS (2018).
  <a href="papers/age_distribution_accepted.pdf">On the age of a randomly picked individual in a linear birth and death process</a>,<br>
<i>J. Appl. Probab.</i> <b>55</b>(1), 82-93. [<a href="https://doi.org/10.1017/jpr.2018.7">Published version</a>]</li>

<li class="vspace"><a href="http://www.stochastik.math.uni-goettingen.de/index.php?id=14&module=Persondetails&range_id=db66fd7fdd6564055fadd6873d86b790&username=jschrie">J&ouml;rn Schrieber</a>, DS and <a href="http://www.stochastik.math.uni-goettingen.de/index.php?id=gottschlich">Carsten Gottschlich</a> (2017).
<a href="http://ieeexplore.ieee.org/document/7784725/">DOTmark - a benchmark for discrete optimal transport</a>,<br>
<i>IEEE Access</i> <b>5</b>, 271-282.</li>

<li class="vspace"><a href="http://www.nathanrossprob.com/">Nathan Ross</a> and DS (2017).
<a href="https://arxiv.org/abs/1606.05825v2">Wireless network signals with moderately correlated shadowing still appear Poisson</a>,<br>
<i>IEEE Trans. Inf. Theory</i> <b>63</b>(2), 1177-1198. [<a href="https://doi.org/10.1109/TIT.2016.2629482">Published version</a>]</li>

<li class="vspace"><a href="http://www.ginsbourger.ch">David Ginsbourger</a>, <a href="http://www.emse.fr/~roustant/">Olivier Roustant</a>, DS, <a href="https://sites.google.com/site/nicolasdurrandehomepage/">Nicolas Durrande</a> and Nicolas Lenz (2016).<br> <a href="http://arxiv.org/abs/1409.6008">On ANOVA decompositions of kernels and Gaussian random field paths</a>,<br>
in <i>Monte Carlo and Quasi-Monte Carlo Methods: MCQMC, Leuven, Belgium, April 2014</i><br>
Ronald Cools and Dirk Nuyens (editors), Springer Proceedings in Mathematics and Statistics <b>163</b>, 315-330.</li>

<li class="vspace">DS, <a href="http://www.stochastik.math.uni-goettingen.de/~sturm/">Anja Sturm</a> and <a href="http://www.math.uni-sb.de/ag-bender/zaehleindex.html">Henryk Z&auml;hle</a> (2016).<br> <a href="http://arxiv.org/abs/1409.4274">On qualitative robustness of the Lotka-Nagaev estimator for the offspring mean of a supercritical Galton-Watson process</a>,<br>
<i>J. Stat. Plan. Inference</i> <b>169</b>, 56--70.</li>

<li class="vspace"><a href="http://www.stochastik.math.uni-goettingen.de/index.php?id=gottschlich">Carsten Gottschlich</a> and DS (2014).<br> <a href="http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0110214">The shortlist method for fast computation of the earth mover's distance and finding optimal solutions to transportation problems</a>,<br>
<i>PLoS ONE</i> <b>9</b>(10), e110214.</li>

<li class="vspace">DS (2014). Stein’s method for approximating complex distributions, with a view towards point processes,<br>
in: <i>Stochastic Geometry, Spatial Statistics and Random Fields, Vol. II: Models and Algorithms</i>,<br>
<a href="http://www.mathematik.uni-ulm.de/stochastik/personal/schmidt/schmidtFrame.html">Volker Schmidt</a> (editor), Springer Lecture Notes in Mathematics <b>2120</b>, 1-30.</li>

<li class="vspace"><a href="http://www.stat.unibe.ch/content/staff/personalhomepages/duembgen/index_eng.html">Lutz Dümbgen</a>, <a href="http://www.kasparrufibach.ch/">Kaspar Rufibach</a> and DS (2014).<br> <a href="http://dx.doi.org/10.1214/14-EJS930">Maximum-likelihood estimation of a log-concave density based on censored data</a>,<br>
<i>Electron. J. Statist.</i> <b>8</b>, 1405-1437.</li>

<li class="vspace"><a href="http://www.imsv.unibe.ch/content/staff/kaspar_stucki">Kaspar Stucki</a> and DS (2014). <a href="http://arxiv.org/abs/1210.4177">Bounds for the probability generating functional of a Gibbs point process</a>,<br> <i>Adv. Appl. Probab.</i> <b>46</b>(1), 21-34.</li>

<li class="vspace">DS and <a href="http://www.imsv.unibe.ch/content/staff/kaspar_stucki">Kaspar Stucki</a> (2014). <a href="http://arxiv.org/abs/1207.3096">Gibbs point process approximation: total variation bounds using Stein’s method</a>,<br> <i>Ann. Probab.</i> <b>42</b>(5), 1911-1951</li>

<li class="vspace"><a href="http://www.stat.unibe.ch/content/staff/personalhomepages/duembgen/index_eng.html">Lutz
  Dümbgen</a>, <a
  href="http://www.statslab.cam.ac.uk/~rjs57/">Richard Samworth</a> and DS (2013).
  <a href="http://arxiv.org/abs/1106.3520">Stochastic search for semiparametric linear regression models</a>,<br>
  in: <i>From Probability to Statistics and Back: High-Dimensional Models and Processes</i>,<br>
  <i>A Festschrift in Honor of Jon Wellner</i>, IMS collections vol. <b>9</b>, 78--90.</li>

<li class="vspace"><a
  href="http://www.stat.unibe.ch/content/staff/personalhomepages/duembgen/index_eng.html">Lutz
  Dümbgen</a>, <a href="http://www.statslab.cam.ac.uk/~rjs57/">Richard Samworth</a> and DS (2011).<br>
  <a href="http://arxiv.org/pdf/1002.3448v4">Approximation by log-concave distributions, with applications to regression</a>, <i>Ann. Statist.</i> <b>39</b>(2), 702--730.<br>
Extended version: Technical report 75, IMSV, University of Bern (<a href="http://arxiv.org/pdf/1002.3448v3">http://arxiv.org/pdf/1002.3448v3</a>).</li>

<li class="vspace">DS, André Hüsler and <a href="http://www.stat.unibe.ch/content/staff/personalhomepages/duembgen/index_eng.html">Lutz Dümbgen</a> (2011). <a href="papers/strm.2011.1073.pdf">Multivariate log-concave distributions as a nearly parametric model</a>,<br>
<i>Statistics &amp; Risk Modeling</i> <b>28</b>(3), 277--295.<br> Original article by kind permission of <a href="http://www.oldenbourg-verlag.de/wissenschaftsverlag/statistics-risk-modeling/21931402">Oldenbourg Wissenschaftsverlag, Munich/Germany</a>.<br>
Extended version: Technical report 74, IMSV, University of Bern (<a href="http://arxiv.org/abs/0907.0250">http://arxiv.org/abs/0907.0250</a>).</li>

<li class="vspace"><a
href="http://school.maths.uwa.edu.au/~adrian/">A. Baddeley</a>, M. Berman, N. Fisher, A. Hardegen, R. Milne, DS, R. Shah, and R. Turner (2010).<br>
<a href="http://dx.doi.org/10.1214/10-EJS581">Spatial logistic regression and change-of-support in Poisson point processes</a>,<br>
<i>Electron. J. Statist.</i> <b>4</b>, 1151--1201.</li>

<li class="vspace">DS and <a href="http://www.stat.unibe.ch/content/staff/personalhomepages/duembgen/index_eng.html">Lutz Dümbgen</a> (2010). <a href="http://dx.doi.org/10.1016/j.spl.2009.11.013">Consistency of multivariate log-concave density estimators</a>,<br>
<i>Statist. Probab. Letters</i> <b>80</b>(5-6), 376--380.</li>

<li class="vspace">DS (2009). <a href="https://projecteuclid.org/journals/bernoulli/volume-15/issue-2/Steins-method-and-Poisson-process-approximation-for-a-class-of/10.3150/08-BEJ161.full">Stein's method and Poisson process approximation for a class of Wasserstein metrics</a>,<br>
<i><a href="http://isi.cbs.nl/bernoulli/">Bernoulli</a></i> <b>15</b>(2), 550--568.</li>

<li class="vspace">DS (2009). <a href="https://doi.org/10.1214/EJP.v14-643">Distance estimates for dependent thinnings of point processes with densities</a>,<br>
<i>Electron. J. Probab.</i> <b>14</b>, 1080--1116.</li>

<li class="vspace">DS and <a href="http://www.ms.unimelb.edu.au/Personnel/profile.php?PC_id=76">Aihua Xia</a>  (2008). <a href="http://arxiv.org/abs/0708.2777"></a>
<a href="https://research-repository.uwa.edu.au/files/1516437/10837_PID10837.pdf">A new metric between distributions of point processes</a>,<br>
<i><a href="http://www.appliedprobability.org/content.aspx?Group=journals&Page=apjournals">Adv. Appl. Probab.</a></i> <b>40</b>(3), 651--672.</li>

<li class="vspace">DS, <a href="http://www.watri.org.au/people-watri/postgrads2.html#BTVo">Ba-Tuong Vo</a> and <a href="http://www.ee.unimelb.edu.au/people/bv/">Ba-Ngu Vo</a> (2008). <a href="papers/ospa.pdf">A consistent metric for performance evaluation of multi-object filters</a>,<br>
<i><a href="http://ewh.ieee.org/soc/sps/tsp/">IEEE Trans. Signal Processing</a></i> <b>56</b>(8, part 1), 3447--3457.</li>

<li class="vspace">DS (2005). Distance estimates for dependent superpositions of point
processes,<br>
<i>Stochastic Process. Appl.</i> <b>115</b>, 1819--1837.
[<a href="papers/superpositions.pdf">Reprint of final version</a>]</li>

<li class="vspace">DS (2005). <a href="https://doi.org/10.1214/EJP.v10-237">Distance estimates for Poisson process approximations of dependent thinnings</a>,<br>
<i>Electron. J. Probab.</i> <b>10</b>, 165--201.</li>

<li class="vspace">DS (2005). <a href="http://projecteuclid.org/euclid.aoap/1107271662">Upper Bounds for Spatial Point Process Approximations</a>,<br>
<i>Ann. Appl. Probab.</i> <b>15</b>, no. 1B, 615--651.<br>&nbsp;</li>
</ul>

<p>&nbsp;</p>
</td></tr></tbody></table>
